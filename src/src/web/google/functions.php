<?php
namespace amekusa\PHPKnives\web\google;
use amekusa\PHPKnives as Kn;

/**
 * Functions communicating with Google API 
 */
interface functions {
	const required = true;
}

function local_domain() {
	return 'google.co.jp';
}
?>